DROP TABLE IF EXISTS venda CASCADE;
DROP TABLE IF EXISTS funcionario CASCADE;
DROP TABLE IF EXISTS cliente CASCADE;
DROP TABLE IF EXISTS carro CASCADE;
DROP TABLE IF EXISTS modelo CASCADE;
DROP TABLE IF EXISTS marca CASCADE;

CREATE TABLE marca(
  cod		INTEGER 	NOT NULL,
  nome		VARCHAR(30)	NOT NULL,
  descricao	TEXT		NOT NULL,
  CONSTRAINT pk_marca
    PRIMARY KEY(cod)
); 
CREATE TABLE  modelo(
  cod			INTEGER		NOT NULL,
  cod_marca 	INTEGER		NOT NULL,
  nome			VARCHAR(30)	NOT NULL,
  descricao      TEXT		NOT NULL,
  CONSTRAINT pk_modelo
    PRIMARY KEY(cod),
  CONSTRAINT fk_modelo_marca
    FOREIGN KEY(cod_marca)
    REFERENCES marca(cod)
);
CREATE TABLE carro(
  cod			INTEGER  		NOT NULL,
  cod_modelo	INTEGER 		NOT NULL,
  cor			VARCHAR(30)		NOT NULL,
  valor			NUMERIC(10,2)	NOT NULL,
  CONSTRAINT pk_carro
    PRIMARY KEY(cod),
  CONSTRAINT fk_carro_modelo
    FOREIGN KEY(cod_modelo)
    REFERENCES modelo(cod)
);
CREATE TABLE cliente(
  cpf		CHAR(15)		NOT NULL,
  nome		VARCHAR(30)		NOT NULL,
  rua		VARCHAR(40)		NOT NULL,
  cidade	VARCHAR(30)		NOT NULL,
  telefone	VARCHAR(30)		NOT NULL,
  CONSTRAINT pk_cliente
    PRIMARY KEY(cpf)
);
CREATE TABLE funcionario(
  cpf		CHAR(15)		NOT NULL,
  nome		VARCHAR(30)		NOT NULL,
  rua		VARCHAR(40)		NOT NULL,
  cidade	VARCHAR(30)		NOT NULL,
  telefone	VARCHAR(30)		NOT NULL,
  email		VARCHAR(30)		NOT NULL,
  senha		VARCHAR(32)		NOT NULL,
  CONSTRAINT pk_funcionario
    PRIMARY KEY(email)
);
CREATE TABLE venda(
  cod			INTEGER			NOT NULL,
  data_venda	DATE			NOT NULL,
  email_func	VARCHAR(30)		NOT NULL,
  cpf_cliente	CHAR(15)		NOT NULL,
  cod_carro		INTEGER			NOT NULL,
  status		CHAR(1)			NOT NULL,
  CONSTRAINT pk_venda
    PRIMARY KEY(cod),
  CONSTRAINT fk_venda_funcionario
    FOREIGN KEY(email_func)
    REFERENCES funcionario(email),
  CONSTRAINT fk_venda_cliente
    FOREIGN KEY(cpf_cliente)
    REFERENCES cliente(cpf),
  CONSTRAINT fk_venda_carro
    FOREIGN KEY(cod_carro)
    REFERENCES carro(cod)
);
INSERT INTO funcionario(cpf,nome,rua,cidade,telefone,email,senha) 
  VALUES ('000.000.000-00','Administrador','','','','admin',MD5('123456'));