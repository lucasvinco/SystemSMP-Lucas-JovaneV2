/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extra;

import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.FrmPrincipal;

/**
 *
 * @author Jovane
 */
public class AtualizaHora extends Thread {

    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    public void run() {
        try {
            while(true){
            FrmPrincipal.lbDataHora.setText("Data\\Hora "+formato.format(Calendar.getInstance().getTime()));
            Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
