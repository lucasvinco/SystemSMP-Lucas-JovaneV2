/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import database.DatabaseFactory;
import domain.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jovane
 */
public class FuncionarioCrud {

    public void inserir(Connection conn, Funcionario funcionario) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO funcionario(cpf,nome,rua,cidade,telefone,email,senha)"+
                    " VALUES(?,?,?,?,?,?,MD5(?));");
            pstm.setString(1, funcionario.getCpf());
            pstm.setString(2, funcionario.getNome());
            pstm.setString(3, funcionario.getRua());
            pstm.setString(4, funcionario.getCidade());
            pstm.setString(5, funcionario.getTelefone());
            pstm.setString(5, funcionario.getEmail());
            pstm.setString(5, funcionario.getSenha());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Funcionario funcionario) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE funcionario"+
                    " SET nome=?,rua=?,cidade=?,telefone=?,email=?,senha=MD5(?)"+
                    " WHERE cpf=?;"
            );

            pstm.setString(1, funcionario.getNome());
            pstm.setString(2, funcionario.getRua());
            pstm.setString(3, funcionario.getCidade());
            pstm.setString(4, funcionario.getTelefone());
            pstm.setString(5, funcionario.getCpf());
            pstm.setString(6, funcionario.getEmail());
            pstm.setString(7, funcionario.getSenha());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Funcionario cliente) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "DELETE FROM funcionario"+
                    " WHERE email = ?;"
            );
            pstm.setString(1, cliente.getCpf());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Funcionario> listar(Connection conn) {
        ArrayList<Funcionario> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cpf,nome,rua,cidade,telefone,email,senha"
                    + "  FROM funcionario;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Funcionario aux = new Funcionario();
                aux.setCpf(rst.getString("cpf"));
                aux.setNome(rst.getString("nome"));
                aux.setRua(rst.getString("rua"));
                aux.setCidade(rst.getString("cidade"));
                aux.setTelefone(rst.getString("telefone"));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Funcionario ler(Connection conn, String email) {
        Funcionario aux = null;

        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cpf,nome,rua,cidade,telefone,email,senha"
                    + "  FROM funcionario"
                    + "  WHERE email=? LIMIT 1;"
            );
            pstm.setString(1, email);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {

                aux.setCpf(rst.getString("cpf"));
                aux.setNome(rst.getString("nome"));
                aux.setRua(rst.getString("rua"));
                aux.setCidade(rst.getString("cidade"));
                aux.setTelefone(rst.getString("telefone"));
                aux.setEmail(rst.getString("email"));
                aux.setSenha(rst.getString("senha"));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }

    public boolean login(Connection conn, String email, String senha) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT email,senha"+
                    " FROM funcionario"+
                    " WHERE email = ?"+
                    " AND senha = MD5(?);"
            );
            pstm.setString(1, email);
            pstm.setString(2, senha);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return false;
    }
    public void rodar(){
        try {
            Connection conn = DatabaseFactory.getDatabase("postgresql").conectar();
            PreparedStatement pstm = conn.prepareStatement(
                    "CREATE TABLE marca(\n"
                    + "  cod		INTEGER 	NOT NULL,\n"
                    + "  nome		VARCHAR(30)	NOT NULL,\n"
                    + "  descricao	TEXT		NOT NULL,\n"
                    + "  CONSTRAINT pk_marca\n"
                    + "    PRIMARY KEY(cod)\n"
                    + "); \n"
                    + " \n"
                    + "CREATE TABLE  modelo(\n"
                    + "  cod			INTEGER		NOT NULL,\n"
                    + "  cod_marca 	INTEGER		NOT NULL,\n"
                    + "  nome			VARCHAR(30)	NOT NULL,\n"
                    + "  descricao      TEXT		NOT NULL,\n"
                    + "  CONSTRAINT pk_modelo\n"
                    + "    PRIMARY KEY(cod),\n"
                    + "  CONSTRAINT fk_modelo_marca\n"
                    + "    FOREIGN KEY(cod_marca)\n"
                    + "    REFERENCES marca(cod)\n"
                    + ");\n"
                    + "\n"
                    + "CREATE TABLE carro(\n"
                    + "  cod			INTEGER  		NOT NULL,\n"
                    + "  cod_modelo	INTEGER 		NOT NULL,\n"
                    + "  cor			VARCHAR(30)		NOT NULL,\n"
                    + "  valor			NUMERIC(10,2)	NOT NULL,\n"
                    + "  CONSTRAINT pk_carro\n"
                    + "    PRIMARY KEY(cod),\n"
                    + "  CONSTRAINT fk_carro_modelo\n"
                    + "    FOREIGN KEY(cod_modelo)\n"
                    + "    REFERENCES modelo(cod)\n"
                    + ");\n"
                    + "CREATE TABLE cliente(\n"
                    + "  cpf		CHAR(15)		NOT NULL,\n"
                    + "  nome		VARCHAR(30)		NOT NULL,\n"
                    + "  rua		VARCHAR(40)		NOT NULL,\n"
                    + "  cidade	VARCHAR(30)		NOT NULL,\n"
                    + "  telefone	VARCHAR(30)		NOT NULL,\n"
                    + "  CONSTRAINT pk_cliente\n"
                    + "    PRIMARY KEY(cpf)\n"
                    + ");\n"
                    + "CREATE TABLE funcionario(\n"
                    + "  cpf		CHAR(15)		NOT NULL,\n"
                    + "  nome		VARCHAR(30)		NOT NULL,\n"
                    + "  rua		VARCHAR(40)		NOT NULL,\n"
                    + "  cidade	VARCHAR(30)		NOT NULL,\n"
                    + "  telefone	VARCHAR(30)		NOT NULL,\n"
                    + "  email		VARCHAR(30)		NOT NULL,\n"
                    + "  senha		VARCHAR(32)		NOT NULL,\n"
                    + "  CONSTRAINT pk_funcionario\n"
                    + "    PRIMARY KEY(email)\n"
                    + ");\n"
                    + "CREATE TABLE venda(\n"
                    + "  cod			INTEGER			NOT NULL,\n"
                    + "  data_venda	DATE			NOT NULL,\n"
                    + "  email_func	VARCHAR(30)		NOT NULL,\n"
                    + "  cpf_cliente	CHAR(15)		NOT NULL,\n"
                    + "  cod_carro		INTEGER			NOT NULL,\n"
                    + "  status		CHAR(1)			NOT NULL,\n"
                    + "  CONSTRAINT pk_venda\n"
                    + "    PRIMARY KEY(cod),\n"
                    + "  CONSTRAINT fk_venda_funcionario\n"
                    + "    FOREIGN KEY(email_func)\n"
                    + "    REFERENCES funcionario(email),\n"
                    + "  CONSTRAINT fk_venda_cliente\n"
                    + "    FOREIGN KEY(cpf_cliente)\n"
                    + "    REFERENCES cliente(cpf),\n"
                    + "  CONSTRAINT fk_venda_carro\n"
                    + "    FOREIGN KEY(cod_carro)\n"
                    + "    REFERENCES carro(cod)\n"
                    + ");\n"
                    + "INSERT INTO funcionario(cpf,nome,rua,cidade,telefone,email,senha) \n"
                    + "  VALUES ('000.000.000-00','Administrador','','','','admin',MD5('123456'));\n"
                    + "           "
            );
            pstm.execute();
            pstm.close();
            conn.close();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());

        }
    }
}
