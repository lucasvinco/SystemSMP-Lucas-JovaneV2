/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import domain.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jovane
 */
public class ClienteCrud {
    public void inserir(Connection conn, Cliente cliente) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO cliente(cpf,nome,rua,cidade,telefone)"
                    + " VALUES(?,?,?,?,?);");
            pstm.setString(1, cliente.getCpf());
            pstm.setString(2, cliente.getNome());
            pstm.setString(3, cliente.getRua());
            pstm.setString(4, cliente.getCidade());
            pstm.setString(5, cliente.getTelefone());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Cliente cliente) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE cliente"+
                    "  SET nome=?,rua=?,cidade=?,telefone=?"+
                    "  WHERE cpf=?;"
            );

            pstm.setString(1, cliente.getNome());
            pstm.setString(2, cliente.getRua());
            pstm.setString(3, cliente.getCidade());
            pstm.setString(4, cliente.getTelefone());
            pstm.setString(5, cliente.getCpf());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Cliente cliente) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
               "DELETE FROM cliente"+
               " WHERE cpf = ?;"
            );
            pstm.setString(1, cliente.getCpf());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Cliente> listar(Connection conn) {
        ArrayList<Cliente> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cpf,nome,rua,cidade,telefone"
                    + "  FROM cliente;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Cliente aux = new Cliente();
                aux.setCpf(rst.getString("cpf"));
                aux.setNome(rst.getString("nome"));
                aux.setRua(rst.getString("rua"));
                aux.setCidade(rst.getString("cidade"));
                aux.setTelefone(rst.getString("telefone"));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Cliente ler(Connection conn, String cpf) {
        Cliente aux = new Cliente();

        try {
            PreparedStatement pstm = conn.prepareStatement(
                "SELECT cpf,nome,rua,cidade,telefone"+
                " FROM cliente"+
                " WHERE cpf = ? LIMIT 1;"
            );
            pstm.setString(1, cpf);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {

                aux.setCpf(rst.getString("cpf"));
                aux.setNome(rst.getString("nome"));
                aux.setRua(rst.getString("rua"));
                aux.setCidade(rst.getString("cidade"));
                aux.setTelefone(rst.getString("telefone"));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }
}
