/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import java.sql.Connection;
import domain.Carro;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jovane
 */
public class CarroCrud {
    public void inserir(Connection conn, Carro carro) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO marca(cod, cod_modelo, cor,valor)"
                    + " VALUES(?,?,?,?);");
            pstm.setInt(1, carro.getCodigo());
            pstm.setInt(2, carro.getModelo().getCod());
            pstm.setString(3, carro.getCor());
            pstm.setFloat(4, carro.getValor());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Carro carro) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE carro "
                    + "  SET cod_modelo=?,cor=?,valor=?"
                    + "  WHERE cod=?;"
            );
            pstm.setInt(1, carro.getModelo().getCod());
            pstm.setString(2, carro.getCor());
            pstm.setFloat(3, carro.getValor());
            pstm.setFloat(4, carro.getCodigo());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Carro carro) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                   "DELETE FROM venda"
                           + "WHERE cod_carro = ?;"
                           + "DELETE carro"
                           + "FROM cod= ?"
            );
            pstm.setInt(1, carro.getCodigo());
            pstm.setInt(2, carro.getCodigo());

            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Carro> listar(Connection conn) {
        ArrayList<Carro> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,cod_modelo,cor,valor"
                    + "  FROM carro;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Carro aux = new Carro();
                aux.setCodigo(rst.getInt("cod"));
                aux.setModelo(new ModeloCrud().ler(conn, rst.getInt("cod_modelo")));
                aux.setCor(rst.getString("cor"));
                aux.setValor(rst.getFloat("valor"));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Carro ler(Connection conn, int cod) {
        Carro aux = null;

        try {
            PreparedStatement pstm = conn.prepareStatement(
                   "SELECT cod,cod_modelo,cor,valor"
                    + "  FROM carro"
                    + "  WHERE cod=? LIMIT 1;"
            );
            pstm.setInt(1, cod);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {
                aux = new Carro();
                aux.setCodigo(rst.getInt("cod"));
                aux.setModelo(new ModeloCrud().ler(conn, rst.getInt("cod_modelo")));
                aux.setCor(rst.getString("cor"));
                aux.setValor(rst.getFloat("valor"));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }

    public int ultimoCad(Connection conn) {
        int aux = 1;
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT * FROM carro ORDER BY cod DESC limit 1;"
            );
            ResultSet rst = pstm.executeQuery();
            if(rst.next()){
                return aux += rst.getInt("cod");
            }else{
                return aux;
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }

    }
}
