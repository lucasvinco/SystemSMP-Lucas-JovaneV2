/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import domain.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Jovane
 */
public class ModeloCrud {
    public void inserir(Connection conn, Modelo modelo) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO modelo(cod,cod_marca,nome,descricao)"
                    + " VALUES(?,?,?,?);");
            pstm.setInt(1, modelo.getCod());
            pstm.setInt(2, modelo.getMarca().getCod());
            pstm.setString(3, modelo.getModelo());
            pstm.setString(4, modelo.getDesc());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Modelo modelo) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE modelo "
                    + "  SET cod_marca=?,nome=?,descricao=?"
                    + "  WHERE cod=?;"
            );
            pstm.setInt(1,modelo.getMarca().getCod());
            pstm.setString(2, modelo.getModelo());
            pstm.setString(3, modelo.getDesc());
            pstm.setInt(4, modelo.getCod());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Modelo modelo) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "   DELETE FROM carro   "
                    + "   WHERE  cod_modelo    = ?;"
                    + " DELETE FROM modelo"
                    + "   WHERE cod_marca      = ?;"

            );
            pstm.setInt(1, modelo.getCod());
            pstm.setInt(2, modelo.getCod());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Modelo> listar(Connection conn) {
        ArrayList<Modelo> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,cod_marca,nome,descricao"
                    + "  FROM modelo;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Modelo aux = new Modelo();
                aux.setCod(rst.getInt("cod"));
                aux.setMarca(new MarcaCrud().ler(conn, rst.getInt("cod_marca")));
                aux.setModelo(rst.getString("nome"));
                aux.setDesc(rst.getString("descricao"));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Modelo ler(Connection conn, int cod) {
        Modelo aux = null;

        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,cod_marca,nome,descricao"
                    + "  FROM modelo"
                    + "  WHERE cod=? LIMIT 1;"
            );
            pstm.setInt(1, cod);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {
                aux = new Modelo();
                aux.setCod(rst.getInt("cod"));
                aux.setMarca(new MarcaCrud().ler(conn, rst.getInt("cod_marca")));
                aux.setModelo(rst.getString("nome"));
                aux.setDesc(rst.getString("descricao"));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }

    public int ultimoCad(Connection conn) {
        int aux = 1;
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT * FROM modelo ORDER BY cod DESC limit 1;"
            );
            ResultSet rst = pstm.executeQuery();
            if(rst.next()){
                return aux += rst.getInt("cod");
            }else{
                return aux;
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }

    }
}
