/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;
import domain.*;
import database.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Jovane
 */
public class VendaCrud {
   public void inserir(Connection conn, Venda venda) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO marca(cod,data_venda,email_func,"
                            + "cpf_cliente,cod_carro,status)"
                    + " VALUES(?,?,?,?,?,?);");
            pstm.setInt(1, venda.getCod());
            pstm.setDate(2, (Date) venda.getData());
            pstm.setString(3, venda.getFuncionario().getEmail());
            pstm.setString(4, venda.getCliente().getCpf());
            pstm.setInt(5, venda.getCarro().getCodigo());
            pstm.setString(6, String.valueOf(venda.getStatus()));
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Venda venda) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE venda "
                    + "  SET data_venda=?,email_func=?,cpf_cliente=?"
                            + ",cod_carro=?,status=?"
                    + "  WHERE cod=?;"
            );
            pstm.setDate(1, (Date) venda.getData());
            pstm.setString(2, venda.getFuncionario().getEmail());
            pstm.setString(3, venda.getCliente().getCpf());
            pstm.setInt(4, venda.getCarro().getCodigo());
            pstm.setString(5, String.valueOf(venda.getStatus()));
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Venda venda) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                  "DELETE FROM venda"
                          + "WHERE cod = ?;"
            );
            pstm.setInt(1, venda.getCod());

            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Venda> listar(Connection conn) {
        ArrayList<Venda> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,data_venda,email_func,"
                            + "cpf_cliente,cod_carro,status"
                    + "  FROM venda;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Venda aux = new Venda();
                aux.setCod(rst.getInt("cod"));
                aux.setData((Date) rst.getDate("data_venda"));
                aux.setFuncionario(new FuncionarioCrud().
                        ler(conn, rst.getString("email_func")));
                aux.setCliente(new ClienteCrud().
                        ler(conn,rst.getString("cpf_cliente")));
                aux.setCarro(new CarroCrud().
                        ler(conn,rst.getInt("cod_carro")));
                aux.setStatus(rst.getString("status").charAt(0));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Venda ler(Connection conn, int cod) {
        Venda aux = null;

        try {
            PreparedStatement pstm = conn.prepareStatement(
                     "SELECT cod,data_venda,email_func,"
                            + "cpf_cliente,cod_carro,status"
                    + "  FROM venda"
                    + "  WHERE cod=? LIMIT 1;"
            );
            pstm.setInt(1, cod);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {

                aux.setCod(rst.getInt("cod"));
                aux.setData((Date) rst.getDate("data_venda"));
                aux.setFuncionario(new FuncionarioCrud().
                        ler(conn, rst.getString("email_func")));
                aux.setCliente(new ClienteCrud().
                        ler(conn,rst.getString("cpf_cliente")));
                aux.setCarro(new CarroCrud().
                        ler(conn,rst.getInt("cod_carro")));
                aux.setStatus(rst.getString("status").charAt(0));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }

    public int ultimoCad(Connection conn) {
        int aux = 1;
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT * FROM venda ORDER BY cod DESC limit 1;"
            );
            ResultSet rst = pstm.executeQuery();
            if(rst.next()){
                return aux += rst.getInt("cod");
            }else{
                return aux;
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }

    }
}
