/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;


import domain.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jovane
 */
public class MarcaCrud {

    public void inserir(Connection conn, Marca marca) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "INSERT INTO marca(cod, nome, descricao)"
                    + " VALUES(?,?,?);");
            pstm.setInt(1, marca.getCod());
            pstm.setString(2, marca.getNome());
            pstm.setString(3, marca.getDesc());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void editar(Connection conn, Marca marca) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "UPDATE marca "
                    + "  SET nome=?, descricao=?"
                    + "  WHERE cod=?;"
            );
            pstm.setString(1, marca.getNome());
            pstm.setString(2, marca.getDesc());
            pstm.setInt(3, marca.getCod());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void delete(Connection conn, Marca marca) {
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "DELETE FROM carro   c\n"
                    + "    USING   modelo  mo,\n"
                    + "            marca   ma\n"
                    + "    WHERE   c.cod_modelo    = mo.cod\n"
                    + "      AND   mo.cod_marca    = ma.cod\n"
                    + "      AND   ma.cod          = ?;\n"
                    + "DELETE FROM modelo\n"
                    + "    WHERE cod_marca         = ?;\n"
                    + "DELETE FROM marca\n"
                    + "    WHERE cod               = ?;"
            );
            pstm.setInt(1, marca.getCod());
            pstm.setInt(2, marca.getCod());
            pstm.setInt(3, marca.getCod());
            pstm.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Marca> listar(Connection conn) {
        ArrayList<Marca> lista = new ArrayList<>();
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,nome,descricao"
                    + "  FROM marca;"
            );
            ResultSet rst = pstm.executeQuery();
            while (rst.next()) {
                Marca aux = new Marca();
                aux.setCod(rst.getInt("cod"));
                aux.setNome(rst.getString("nome"));
                aux.setDesc(rst.getString("descricao"));
                lista.add(aux);
            }
            return lista;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return lista;
        }
    }

    public Marca ler(Connection conn, int cod) {
        Marca aux = null;

        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT cod,nome,descricao"
                    + "  FROM marca"
                    + "  WHERE cod=? LIMIT 1;"
            );
            pstm.setInt(1, cod);
            ResultSet rst = pstm.executeQuery();

            if (rst.next()) {
                aux = new Marca();
                aux.setCod(rst.getInt("cod"));
                aux.setNome(rst.getString("nome"));
                aux.setDesc(rst.getString("descricao"));
                return aux;
            } else {
                return aux;
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }
    }

    public int ultimoCad(Connection conn) {
        int aux = 1;
        try {
            PreparedStatement pstm = conn.prepareStatement(
                    "SELECT * FROM marca ORDER BY cod DESC limit 1;"
            );
            ResultSet rst = pstm.executeQuery();
            if(rst.next()){
                return aux += rst.getInt("cod");
            }else{
                return aux;
            }

        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return aux;
        }

    }
}
