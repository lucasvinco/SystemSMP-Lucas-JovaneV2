/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

/**
 *
 * @author Jovane
 */
public class Marca {
    public static int next = 1;
    private int     cod;
    private String  nome;
    private String  desc;

    public  int getNext() {
        return next;
    }

    public static void setNext(int next) {
        Marca.next = next;
    }

    public Marca(){
    }
    public Marca(String nome){
       this.cod = next;
       next++;
       this.nome = nome;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
